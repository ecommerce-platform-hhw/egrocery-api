module.exports = async (ctx, next) => {
  if (ctx.request.header["x-auth-neotenant"]) {
    ctx.query["tenant_id"] = ctx.request.header["x-auth-neotenant"];
    ctx.request.body.tenant_id = ctx.request.header["x-auth-neotenant"];
    // Go to next policy or will reach the controller's action.
    return await next();
  }
  ctx.unauthorized(`Tenant id not present in header`);
};
