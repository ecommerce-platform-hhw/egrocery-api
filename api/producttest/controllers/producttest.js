"use strict";

module.exports = {
  build: async (ctx) => {
    let product = await strapi.services.producttest.findOne({
      id: ctx.params.id,
    });

    if (!product.attributes.length) return;

    const cartesian = (sets) => {
      return sets.reduce(
        (acc, curr) => {
          return acc
            .map((x) => {
              return curr.map((y) => {
                return x.concat([y]);
              });
            })
            .flat();
        },
        [[]]
      );
    };

    //capitalize function
    const capitalize = (s) => {
      if (typeof s !== "string") return "";
      return s
        .split(" ")
        .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
        .join(" ");
    };

    const { attributes } = product;

    //map functions return an array of array [["sm","md"],["red","green"]]
    // const variations = cartesian(_.map(attributes,({name,options}) => _.map(options, ({value}) => ({[name]:value}))))
    const variations = cartesian(
      _.map(attributes, ({ name, options }) =>
        _.map(options, ({ value }) => ({ [name]: value }))
      )
    );

    //iterate through all variations creating the records
    const records = _.map(variations, (variation) => {
      let name = variation.reduce(
        (acc, current) => acc + " " + Object.values(current)[0],
        product.name
      );
      let slug = variation
        .reduce(
          (acc, current) =>
            acc + "-" + Object.values(current)[0].replace(/ /g, "-"),
          product.slug
        )
        .toLowerCase();

      return {
        producttest: product.id,
        name: capitalize(name),
        slug: slug,
        price: product.price,
        description: product.description,
        stock: product.stock,
        attributes: variation,
        ...("sale" in product && { sale: product.sale }),
      };
    });

    // creating all records with variations

    try {
      const createAllRecords = await Promise.all(
        records.map(
          (record) =>
            new Promise(async (resolve, reject) => {
              try {
                const created = await strapi.services.variation.create(record);
                resolve(created);
              } catch (err) {
                reject(err);
              }
            })
        )
      );

      ctx.send(createAllRecords);
    } catch (err) {
      console.log(err);
    }
  },
};
