"use strict";
const { parseMultipartData, sanitizeEntity } = require("strapi-utils");

module.exports = {
  /**
   * Retrieve records.
   *
   * @return {Object}
   */
  /**
   * Retrieve a record.
   *
   * @return {Object}
   */
  find: async (ctx) => {
    ctx.query["user.id"] = ctx.state.user.id;
    let entity = await strapi.query("product-relationship").find(ctx.query, []);

    return sanitizeEntity(entity, {
      model: strapi.models["product-relationship"],
    });
  },

  /**
   * Retrieve a record.
   *
   * @return {Object}
   */
  /**
   * Retrieve a record.
   *
   * @return {Object}
   */

  async findOne(ctx) {
    const { id } = ctx.params;

    const entity = await strapi.services["product-relationship"].findOne({
      id,
    });
    return sanitizeEntity(entity, {
      model: strapi.models["product-relationship"],
    });
  },

  /**
   * Create a record.
   *
   * @return {Object}
   */

  async create(ctx) {
    let entity;
    if (ctx.is("multipart")) {
      const { data, files } = parseMultipartData(ctx);
      entity = await strapi.services["product-relationship"].create(data, {
        files,
      });
    } else {
      entity = await strapi.services["product-relationship"].create(
        ctx.request.body
      );
    }
    return sanitizeEntity(entity, {
      model: strapi.models["product-relationship"],
    });
  },

  /**
   * Update a record.
   *
   * @return {Object}
   */

  async update(ctx) {
    const { id } = ctx.params;

    let entity;
    if (ctx.is("multipart")) {
      const { data, files } = parseMultipartData(ctx);
      entity = await strapi.services["product-relationship"].update(
        { id },
        data,
        {
          files,
        }
      );
    } else {
      entity = await strapi.services["product-relationship"].update(
        { id },
        ctx.request.body
      );
    }

    return sanitizeEntity(entity, {
      model: strapi.models["product-relationship"],
    });
  },

  /**
   * Delete a record.
   *
   * @return {Object}
   */

  async delete(ctx) {
    const { id } = ctx.params;

    const entity = await strapi.services["product-relationship"].delete({ id });
    return sanitizeEntity(entity, {
      model: strapi.models["product-relationship"],
    });
  },

  async createMany(ctx) {
    if (Array.isArray(ctx.request.body)) {
      let dataBody = ctx.request.body;
      return await Promise.all(
        dataBody.map(strapi.services["product-relationship"].create)
      );
    } else {
      return strapi.services["product-relationship"].create(ctx.request.body);
    }
  },
};
