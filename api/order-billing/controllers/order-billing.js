"use strict";
const { parseMultipartData, sanitizeEntity } = require("strapi-utils");

module.exports = {
  /**
   * Retrieve records.
   *
   * @return {Object}
   */
  /**
   * Retrieve a record.
   *
   * @return {Object}
   */
  find: async (ctx) => {
    ctx.query["user.id"] = ctx.state.user.id;
    let entity = await strapi.query("order-billing").find(ctx.query, []);

    return sanitizeEntity(entity, {
      model: strapi.models["order-billing"],
    });
  },

  /**
   * Retrieve a record.
   *
   * @return {Object}
   */
  /**
   * Retrieve a record.
   *
   * @return {Object}
   */

  async findOne(ctx) {
    const { id } = ctx.params;

    const entity = await strapi.services["order-billing"].findOne({ id });
    return sanitizeEntity(entity, {
      model: strapi.models["order-billing"],
    });
  },

  /**
   * Create a record.
   *
   * @return {Object}
   */

  async create(ctx) {
    let entity;
    if (ctx.is("multipart")) {
      const { data, files } = parseMultipartData(ctx);
      entity = await strapi.services["order-billing"].create(data, {
        files,
      });
    } else {
      entity = await strapi.services["order-billing"].create(ctx.request.body);
    }
    return sanitizeEntity(entity, {
      model: strapi.models["order-billing"],
    });
  },

  /**
   * Update a record.
   *
   * @return {Object}
   */

  async update(ctx) {
    const { id } = ctx.params;

    let entity;
    if (ctx.is("multipart")) {
      const { data, files } = parseMultipartData(ctx);
      entity = await strapi.services["order-billing"].update({ id }, data, {
        files,
      });
    } else {
      entity = await strapi.services["order-billing"].update(
        { id },
        ctx.request.body
      );
    }

    return sanitizeEntity(entity, {
      model: strapi.models["order-billing"],
    });
  },

  /**
   * Delete a record.
   *
   * @return {Object}
   */

  async delete(ctx) {
    const { id } = ctx.params;

    const entity = await strapi.services["order-billing"].delete({ id });
    return sanitizeEntity(entity, {
      model: strapi.models["order-billing"],
    });
  },

  async createMany(ctx) {
    if (Array.isArray(ctx.request.body)) {
      let dataBody = ctx.request.body;
      return await Promise.all(
        dataBody.map(strapi.services["order-billing"].create)
      );
    } else {
      return strapi.services["order-billing"].create(ctx.request.body);
    }
  },
};
