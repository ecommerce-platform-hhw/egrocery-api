"use strict";
const { parseMultipartData, sanitizeEntity } = require("strapi-utils");
var _ = require("underscore");
var uniqid = require("uniqid");
Object.defineProperty(Array.prototype, "flat", {
  value: function (depth = 1) {
    return this.reduce(function (flat, toFlatten) {
      return flat.concat(
        Array.isArray(toFlatten) && depth > 1
          ? toFlatten.flat(depth - 1)
          : toFlatten
      );
    }, []);
  },
});

module.exports = {
  indexers: async (ctx) => {
    ctx.send({ status: 400, message: "success" });
  },
  create: async (ctx) => {
    let productObj = ctx.request.body;
    productObj.user = ctx.state.user.id;
    productObj.type = "simple";

    let splitProductName = productObj.name.split(" ");
    //create product slug
    productObj.slug = uniqid(
      splitProductName
        .reduce((acc, current) => acc + "-" + current)
        .toLowerCase() + "-"
    );

    let createdProduct = await strapi.services.product.create(productObj);

    //create slug for simple type
    let slug = uniqid(
      splitProductName
        .reduce((acc, current) => acc + "-" + current)
        .toLowerCase() + "-"
    );

    //capitalize function
    const capitalize = (s) => {
      if (typeof s !== "string") return "";
      return s
        .split(" ")
        .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
        .join(" ");
    };

    //create simple main product type
    let productMainVariation = {
      product: createdProduct.id,
      name: capitalize(createdProduct.name),
      slug: slug,
      price: createdProduct.price,
      description: createdProduct.description,
      short_description: createdProduct.short_description,
      type: "simple",
    };

    let createdProductMainVariation = await strapi.services.variation.create(
      productMainVariation
    );

    return sanitizeEntity(createdProduct, { model: strapi.models.product });
  },
  update: async (ctx) => {
    let id = ctx.params.id;
    let shop = ctx.request.body.shop;

    let mainVariation = {};
    let {
      name,
      price,
      discount,
      stock,
      description,
      short_description,
      status,
    } = (mainVariation = ctx.request.body);

    let entity = await strapi.services.product.update(
      { id, shop },
      ctx.request.body
    );

    let varEntity = await strapi.services.variation.update(
      { product: id, type: "simple" },
      { name, price, discount, stock, description, short_description, status }
    );

    if (ctx.request.body.product_relationship_tmp.length > 0) {
      await Promise.all(
        ctx.request.body.product_relationship_tmp
          .filter((res) => res.type !== "delete")
          .map(strapi.services["product-relationship"].create)
      );

      let markedToBeDeleted = ctx.request.body.product_relationship_tmp.filter(
        (res) => res.type === "delete"
      );
      if (markedToBeDeleted.length > 0) {
        await Promise.all(
          markedToBeDeleted.map((res) => {
            let id = res.id;
            strapi.services["product-relationship"].delete({ id }, res);
          })
        );
      }
    }

    return sanitizeEntity(entity, { model: strapi.models.product });
  },
  find: async (ctx) => {
    let limit = 10;
    let start = 0;
    ctx.query._start = ctx.query._start ? ctx.query._start - 1 : start;
    ctx.query._limit = limit;

    if (ctx.query._q) {
      let entity = await strapi.query("product").search(ctx.query);
      let sanitizedEntity = sanitizeEntity(entity, {
        model: strapi.models.product,
      });
      return entity;
    }

    let entity = await strapi
      .query("product")
      .find(ctx.query, [
        "categories",
        "categories.name",
        "variations",
        "variations.images",
        "variations.images.image",
        "images",
        "images.image",
        "shop",
        "shop.name",
        "product_relationships",
        "product_relationships.related_product",
        "product_relationships.related_product.images.image",
      ]);

    let productRes = [];
    entity.forEach((product) => {
      product.variations.map((variation) => {
        if (variation.discount_active) {
          variation.final_price = variation.discount;
        } else {
          variation.final_price = variation.price;
        }
        return variation;
      });

      productRes.push(product);
    });

    return productRes;
  },
  findOwner: (ctx) => {
    ctx.query["shop.id"] = ctx.params.shopId;
    return strapi
      .query("product")
      .find(ctx.query, [
        "category",
        "category.name",
        "variations",
        "variations.images",
        "variations.images.image",
        "images",
        "images.image",
        "shop",
        "shop.name",
        "product_relationships",
        "product_relationships.related_product",
        "product_relationships.related_product.images.image",
      ]);
  },
  findAdminVendorProduct: (ctx) => {
    ctx.query["shop.id"] = ctx.params.shopId;
    return strapi
      .query("product")
      .find(ctx.query, [
        "category",
        "category.name",
        "variations",
        "variations.images",
        "variations.images.image",
        "images",
        "images.image",
        "shop",
        "shop.name",
        "product_relationships",
        "product_relationships.related_product",
        "product_relationships.related_product.images.image",
      ]);
  },
  findOne: async (ctx) => {
    let product = await strapi
      .query("product")
      .findOne({ id: ctx.params.id }, [
        "category",
        "category.name",
        "variations",
        "variations.images",
        "variations.images.image",
        "images",
        "shop",
        "shop.name",
        "images.image",
        "product_relationships",
        "product_relationships.related_product",
        "product_relationships.related_product.images.image",
      ]);

    let productRes = await product.variations.map((variation) => {
      if (variation.discount_active) {
        variation.final_price = variation.discount;
      } else {
        variation.final_price = variation.price;
      }
      return variation;
    });

    return product;
  },
  findOneOwner: (ctx) => {
    return strapi
      .query("product")
      .findOne({ id: ctx.params.id, "shop.id": ctx.params.shopId }, [
        "categories",
        "variations",
        "variations.images",
        "variations.images.image",
        "images",
        "images.image",
        "shop",
        "shop.name",
        "product_relationships",
        "product_relationships.related_product",
        "product_relationships.related_product.images.image",
      ]);
  },
  findOneAdminVendorProduct: (ctx) => {
    ctx.query["shop.id"] = ctx.params.shopId;
    return strapi
      .query("product")
      .findOne({ id: ctx.params.id, "shop.id": ctx.params.shopId }, [
        "category",
        "category.name",
        "variations",
        "variations.images",
        "variations.images.image",
        "images",
        "images.image",
        "shop",
        "shop.name",
        "product_relationships",
        "product_relationships.related_product",
        "product_relationships.related_product.images.image",
      ]);
  },
  build: async (ctx) => {
    let product = await strapi.services.product.findOne({ id: ctx.params.id });

    if (!product.pattributes.length) return;

    const cartesian = (sets) => {
      return sets.reduce(
        (acc, curr) => {
          return acc
            .map((x) => {
              return curr.map((y) => {
                return x.concat([y]);
              });
            })
            .flat();
        },
        [[]]
      );
    };

    //capitalize function
    const capitalize = (s) => {
      if (typeof s !== "string") return "";
      return s
        .split(" ")
        .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
        .join(" ");
    };

    //function to return array of array
    function arrayOfArrayAttributes(attributes) {
      return _.map(attributes, ({ name, options, variation }) => {
        if (variation)
          return _.map(options, ({ value }) => ({ [name]: value }));
      });
    }

    const { pattributes } = product;

    //map functions return an array of array [["sm","md"],["red","green"]]
    const variations = cartesian(
      //filter variation attributes
      arrayOfArrayAttributes(
        pattributes.filter((pattribute) => {
          return pattribute.variation == true;
        })
      )
    );
    //  const variations = cartesian(_.map(attributes, ({name, options}) => _.map(options, ({value}) => ({ [name]: value }) )))

    //iterate through all variations creating the records
    const records = _.map(variations, (variation) => {
      let name = variation.reduce(
        (acc, current) => acc + " " + Object.values(current)[0],
        product.name
      );
      let slug = variation
        .reduce(
          (acc, current) =>
            acc + "-" + Object.values(current)[0].replace(/ /g, "-"),
          product.slug
        )
        .toLowerCase();

      return {
        product: product.id,
        name: capitalize(name),
        slug: slug,
        price: product.price,
        description: product.description,
        stock: product.stock,
        pattributes: variation,
        type: "variable",
        ...("sale" in product && { sale: product.sale }),
      };
    });

    // creating all records with variations

    try {
      const createAllRecords = await Promise.all(
        records.map(
          (record) =>
            new Promise(async (resolve, reject) => {
              try {
                const created = await strapi.services.variation.create(record);
                resolve(created);
              } catch (err) {
                reject(err);
              }
            })
        )
      );

      ctx.send(createAllRecords);
    } catch (err) {
      console.log(err);
    }
  },
  findAllCategoriesForProduct: async (ctx) => {
    let entities;

    let knex = strapi.connections.default;

    const raw = `WITH RECURSIVE cats(product_id, category_id, category_name, parent,category_slug) AS (
              SELECT cpp.product_id,c.id,c.name,c.parent,c.slug
              FROM categories_products__products_categories cpp, categories c
              WHERE category_id = c.id
            UNION ALL
              SELECT product_id,cs.id,cs.name,cs.parent,cs.slug
              FROM cats, categories cs
              WHERE cats.parent = cs.id
            )
            SELECT category_name as name,category_slug as slug FROM cats LEFT JOIN products p ON product_id = p.id WHERE p.slug = ?; `;

    let result = await knex.raw(raw, [ctx.params.name]);

    entities = result.rows;

    return entities;
  },
};
