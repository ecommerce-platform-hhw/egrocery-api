'use strict';
const { parseMultipartData, sanitizeEntity } = require('strapi-utils');

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
    create: async ctx =>{ 
        let reviewObj = ctx.request.body; 
        reviewObj.status = 'active';
        reviewObj.user = ctx.state.user.id;

        let productData = await strapi.query('product').findOne({id:reviewObj.product},['shop'])

            if(!productData)
                return ctx.send({status:'success'})

        reviewObj.shop = productData.shop.id;        

        let createdReview= await strapi.services['product-review'].create(reviewObj);
        return sanitizeEntity(createdReview, { model: strapi.models['product-review'] });
      },
      updateAdminReply: async ctx =>{ 
        let id = ctx.params.id; 
        let reviewObj = ctx.request.body;  
             

        let updatedReview= await strapi.services['product-review'].update({id:id,tenant_id: ctx.query['tenant_id']},reviewObj);
        return sanitizeEntity(updatedReview, { model: strapi.models['product-review'] });
      },
};
