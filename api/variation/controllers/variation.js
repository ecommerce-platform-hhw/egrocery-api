"use strict";
const { parseMultipartData, sanitizeEntity } = require("strapi-utils");

module.exports = {
  /**
   * Retrieve records.
   *
   * @return {Object}
   */
  /**
   * Retrieve a record.
   *
   * @return {Object}
   */
  async find(ctx) {
    let entities;
    if (ctx.query._q) {
      entities = await strapi.services.variation.search(ctx.query);
    } else {
      entities = await strapi.services.variation.find(ctx.query);
    }

    return entities.map((entity) =>
      sanitizeEntity(entity, { model: strapi.models.variation })
    );
  },
  /**
   * Retrieve a record.
   *
   * @return {Object}
   */
  /**
   * Retrieve a record.
   *
   * @return {Object}
   */

  async findOne(ctx) {
    const { id } = ctx.params;

    const entity = await strapi.services.variation.findOne({ id });
    return sanitizeEntity(entity, { model: strapi.models.variation });
  },

  /**
   * Create a record.
   *
   * @return {Object}
   */

  async create(ctx) {
    let entity;
    if (ctx.is("multipart")) {
      const { data, files } = parseMultipartData(ctx);
      entity = await strapi.services.variation.create(data, { files });
    } else {
      entity = await strapi.services.variation.create(ctx.request.body);
    }
    return sanitizeEntity(entity, { model: strapi.models.variation });
  },

  /**
   * Update a record.
   *
   * @return {Object}
   */

  async update(ctx) {
    const { id } = ctx.params;

    let entity;
    if (ctx.is("multipart")) {
      const { data, files } = parseMultipartData(ctx);
      entity = await strapi.services.variation.update({ id }, data, {
        files,
      });
    } else {
      entity = await strapi.services.variation.update({ id }, ctx.request.body);
    }

    return sanitizeEntity(entity, { model: strapi.models.variation });
  },

  /**
   * Delete a record.
   *
   * @return {Object}
   */

  async delete(ctx) {
    const { id } = ctx.params;

    const entity = await strapi.services.variation.delete({ id });
    return sanitizeEntity(entity, { model: strapi.models.variation });
  },

  async createMany(ctx) {
    if (Array.isArray(ctx.request.body)) {
      let dataBody = ctx.request.body;
      return await Promise.all(dataBody.map(strapi.services.variation.create));
    } else {
      return strapi.services.variation.create(ctx.request.body);
    }
  },
  async editMany(ctx) {
    if (Array.isArray(ctx.request.body)) {
      let dataBody = ctx.request.body;
      return await Promise.all(
        dataBody.map((res) => {
          let id = res.id;
          strapi.services.variation.update({ id }, res);
        })
      );
    } else {
      let id = ctx.request.body.id;
      return strapi.services.variation.update({ id }, ctx.request.body);
    }
  },
};
