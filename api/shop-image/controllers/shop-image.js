"use strict";
const { parseMultipartData, sanitizeEntity } = require("strapi-utils");

module.exports = {
  /**
   * Retrieve records.
   *
   * @return {Object}
   */
  /**
   * Retrieve a record.
   *
   * @return {Object}
   */
  find: async (ctx) => {
    ctx.query["user.id"] = ctx.state.user.id;
    let entity = await strapi.query("shop-image").find(ctx.query, []);

    return sanitizeEntity(entity, {
      model: strapi.models["shop-image"],
    });
  },

  /**
   * Retrieve a record.
   *
   * @return {Object}
   */
  /**
   * Retrieve a record.
   *
   * @return {Object}
   */

  async findOne(ctx) {
    const { id } = ctx.params;

    const entity = await strapi.services["shop-image"].findOne({ id });
    return sanitizeEntity(entity, {
      model: strapi.models["shop-image"],
    });
  },

  /**
   * Create a record.
   *
   * @return {Object}
   */

  async create(ctx) {
    let entity;
    if (ctx.is("multipart")) {
      const { data, files } = parseMultipartData(ctx);
      entity = await strapi.services["shop-image"].create(data, {
        files,
      });
    } else {
      entity = await strapi.services["shop-image"].create(ctx.request.body);
    }
    return sanitizeEntity(entity, {
      model: strapi.models["shop-image"],
    });
  },

  /**
   * Update a record.
   *
   * @return {Object}
   */

  async update(ctx) {
    const { id } = ctx.params;

    let entity;
    if (ctx.is("multipart")) {
      const { data, files } = parseMultipartData(ctx);
      entity = await strapi.services["shop-image"].update({ id }, data, {
        files,
      });
    } else {
      entity = await strapi.services["shop-image"].update(
        { id },
        ctx.request.body
      );
    }

    return sanitizeEntity(entity, {
      model: strapi.models["shop-image"],
    });
  },

  /**
   * Delete a record.
   *
   * @return {Object}
   */

  async delete(ctx) {
    const { id } = ctx.params;

    const entity = await strapi.services["shop-image"].delete({ id });
    return sanitizeEntity(entity, {
      model: strapi.models["shop-image"],
    });
  },

  async createMany(ctx) {
    if (Array.isArray(ctx.request.body)) {
      let dataBody = ctx.request.body;
      return await Promise.all(
        dataBody.map(strapi.services["shop-image"].create)
      );
    } else {
      return strapi.services["shop-image"].create(ctx.request.body);
    }
  },
};
