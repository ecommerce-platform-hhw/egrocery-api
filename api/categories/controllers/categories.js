"use strict";
const { parseMultipartData, sanitizeEntity } = require("strapi-utils");
var slugify = require("slugify");
var uniqid = require("uniqid");
var _ = require("underscore");

module.exports = {
  find: (ctx) => {
    return strapi.query("categories").find(ctx.query, ["image"]);
  },
  findTree: async (ctx) => {
    let knex = strapi.connections.default;

    const raw = `WITH RECURSIVE children AS (
  SELECT id,parent, name
  FROM categories where parent is NULL AND tenant_id = ?
 UNION
  SELECT tp.id, tp.parent, tp.name
  FROM categories tp
  JOIN children c ON tp.parent = c.id
 )
 SELECT *
 FROM children;`;

    let result = await knex.raw(raw, [ctx.query["tenant_id"]]);

    let entities = result.rows;

    //unflatten categories
    let unflatten = function (array, parent, tree) {
      tree = typeof tree !== "undefined" ? tree : [];
      parent = typeof parent !== "undefined" ? parent : { id: null };

      var children = _.filter(array, function (child) {
        return child.parent == parent.id;
      });

      if (!_.isEmpty(children)) {
        if (parent.id == null) {
          tree = children;
        } else {
          parent["isExpanded"] = true;
          parent["children"] = children;
        }
        _.each(children, function (child) {
          unflatten(array, child);
        });
      }

      return tree;
    };

    let nestedData = unflatten(entities);

    return nestedData;
  },

  findAllProductsForCategory: async (ctx) => {
    let entities;

    let knex = strapi.connections.default;

    const createFuncRaw = `CREATE OR REPLACE FUNCTION productsUnderCategory(cid integer)
            RETURNS TABLE (products varchar(50),descr  text) AS
            $func$
              WITH RECURSIVE prods(category_id, parent, product_id, product,descr) AS (
                  SELECT category_id, parent, product_id, products.name,products.description
                  FROM categories_products__products_categories, products, categories
                  WHERE product_id = products.id AND category_id = categories.id
                UNION ALL
                  SELECT par.id, par.parent, product_id, product,descr
                 FROM prods, categories AS par
                 WHERE prods.parent = par.id
              )
             SELECT product,descr FROM prods WHERE cid = category_id
           $func$ LANGUAGE sql; `;
    const funcRaw = `SELECT productsUnderCategory(id) AS products FROM categories WHERE name = ? ;`;

    const raw = `WITH RECURSIVE prods(category_id, parent, product_ids, producto,descr,category_slug,product_slug) AS (
          SELECT category_id, parent, product_id, products.name,products.description,categories.slug,products.slug
          FROM categories_products__products_categories, products, categories
          WHERE product_id = products.id AND category_id = categories.id
        UNION ALL
          SELECT par.id, par.parent, product_ids, producto,descr,par.slug,product_slug
         FROM prods, categories AS par
         WHERE prods.parent = par.id
      )
      SELECT product_ids as id,product_slug as slug, producto as name,json_agg(DISTINCT jsonb_build_object('name',category_slug)) as categories,json_agg(DISTINCT jsonb_build_object('image',uf.*)) images,json_agg(DISTINCT v.*) variations FROM prods  LEFT JOIN products__images pi ON pi.product_id = product_id  LEFT JOIN upload_file_morph um ON um.related_id = "pi"."shop-image_id"
      LEFT JOIN upload_file uf ON uf.id = um.upload_file_id
      LEFT JOIN variations v ON v.product = product_id
      WHERE category_slug  = :cat_name AND "pi"."product_id"= product_ids
      GROUP BY product_ids,producto,category_slug,product_slug ORDER BY product_ids DESC LIMIT :limit OFFSET :offset`;

    let result = await knex.raw(raw, {
      cat_name: ctx.params.name,
      offset: ctx.query._start - 1,
      limit: 10,
    });

    entities = result.rows;

    return entities;
  },

  countAllProductsForCategory: async (ctx) => {
    let entities;

    let knex = strapi.connections.default;

    const raw = `WITH RECURSIVE prods(category_id, parent, product_ids, producto,descr,category_slug,product_slug) AS (
          SELECT category_id, parent, product_id, products.name,products.description,categories.slug,products.slug
          FROM categories_products__products_categories, products, categories
          WHERE product_id = products.id AND category_id = categories.id
        UNION ALL
          SELECT par.id, par.parent, product_ids, producto,descr,par.slug,product_slug
         FROM prods, categories AS par
         WHERE prods.parent = par.id
      )
      SELECT COUNT(DISTINCT product_ids) as count FROM prods 
      WHERE category_slug  = :cat_name`;
    let result = await knex.raw(raw, { cat_name: ctx.params.name });

    entities = result.rows;

    return entities;
  },

  create: async (ctx) => {
    let categoryObj = ctx.request.body;
    categoryObj.slug = uniqid(
      slugify(categoryObj.name, { replacement: "-", lower: true }) + "-"
    );
    categoryObj.code = slugify(categoryObj.name, {
      replacement: "-",
      lower: true,
    }).toUpperCase();
    categoryObj.status = "active";
    let createdCategory = await strapi.services.categories.create(categoryObj);
    return sanitizeEntity(createdCategory, { model: strapi.models.categories });
  },
  update: async (ctx) => {
    const { id } = ctx.params;
    let categoryObj = ctx.request.body;
    categoryObj.slug = uniqid(
      slugify(categoryObj.name, { replacement: "-", lower: true }) + "-"
    );
    categoryObj.code = slugify(categoryObj.name, {
      replacement: "-",
      lower: true,
    }).toUpperCase();
    categoryObj.status = "active";
    let createdCategory = await strapi.services.categories.update(
      { id: id, tenant_id: ctx.request.header["x-auth-neotenant"] },
      categoryObj
    );
    return sanitizeEntity(createdCategory, { model: strapi.models.categories });
  },
};
