"use strict";

/**
 * `Email` service.
 */

const nodemailer = require("nodemailer");
var hbs = require("nodemailer-express-handlebars");
var path = require("path");

// Create reusable transporter object using SMTP transport.
const transporter = nodemailer.createTransport({
  service: process.env.MAIL_HOST,
  auth: {
    user: process.env.MAIL_USER,
    pass: process.env.MAIL_PASSWORD,
  },
});

module.exports = {
  send: (from, to, subject, text) => {
    const handlebarsOptions = {
      viewEngine: {
        extName: ".html",
        partialsDir: path.resolve("./templates/email/"),
        layoutsDir: path.resolve("./templates/email/"),
        defaultLayout: "reset-password.html",
      },
      viewPath: path.resolve("./templates/email/"),
      extName: ".html",
    };

    transporter.use("compile", hbs(handlebarsOptions));

    // Setup e-mail data.
    const options = {
      from,
      to,
      subject,
      text,
      template: "reset-password",
      context: {
        vendor: "Neocommerce Limited",
      },
    };

    // Return a promise of the function that sends the email.
    return transporter.sendMail(options);
  },
};
