"use strict";
const { parseMultipartData, sanitizeEntity } = require("strapi-utils");
/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  createStaff: async (ctx) => {
    const userEntity = await strapi.plugins[
      "users-permissions"
    ].services.user.fetchAll({ email: ctx.request.body.email });

    if (userEntity.length === 0) {
      ctx.unauthorized(
        `User not found. Let user sign up before adding to shop. This request will be handled properly in the future.`
      );
      return;
    }

    let staffObj = {
      user: { id: userEntity[0].id },
      app_role: { id: ctx.request.body.role_select },
      shop: { id: ctx.params.shopId },
      tenant_id: ctx.request.body.tenant_id,
    };

    let createdStaff = await strapi.services["user-app-role"].create(staffObj);
    return { id: createdStaff.id, app_role: createdStaff.app_role };
  },

  findStaffDetailsForShop: async (ctx) => {
    ctx.query["id"] = ctx.params.id;
    ctx.query["shop.id"] = ctx.params.shopId;
    return strapi.query("user-app-role").findOne(ctx.query);
  },
  updateStaffRoleForShop: async (ctx) => {
    let id = ctx.params.id;
    let entity = await strapi.services["user-app-role"].update(
      { id },
      { app_role: { id: ctx.request.body.role } }
    );
    return entity.app_role;
  },
  deleteStaffForShop: async (ctx) => {
    let id = ctx.params.id;
    let entity = await strapi.services["user-app-role"].delete({ id });
    return entity.app_role;
  },
};
