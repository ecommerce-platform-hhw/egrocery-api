"use strict";
const { parseMultipartData, sanitizeEntity } = require("strapi-utils");
var slugify = require("slugify");
var uniqid = require("uniqid");
/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  findUserShops: (ctx) => {
    // ctx.query['owner.id']= ctx.state.user.id;
    ctx.query["staff.id"] = ctx.state.user.id;
    return strapi.query("shop").find(ctx.query);
  },
  findUserShop: (ctx) => {
    // ctx.query['owner.id']= ctx.state.user.id;
    ctx.query["staff.id"] = ctx.state.user.id;
    ctx.query.id = ctx.params.id;
    return strapi.query("shop").findOne(ctx.query);
  },
  findAdminShops: (ctx) => {
    // ctx.query['owner.id']= ctx.state.user.id;
    // ctx.query['tenant_id']= ctx.state.user.id;
    return strapi.query("shop").find(ctx.query);
  },
  findOneAdminShop: (ctx) => {
    ctx.query.id = ctx.params.id;
    return strapi.query("shop").findOne(ctx.query);
  },
  create: async (ctx) => {
    let shopObj = ctx.request.body;
    shopObj.slug = uniqid(
      slugify(shopObj.name, { replacement: "-", lower: true }) + "-"
    );
    shopObj.owner = ctx.state.user.id;
    shopObj.staff = ctx.state.user.id;

    shopObj.tenant_id = ctx.request.header["x-auth-neotenant"];
    let createdShop = await strapi.services.shop.create(shopObj);
    let sanitizedCreatedShop = sanitizeEntity(createdShop, {
      model: strapi.models.shop,
    });
    // return sanitizeEntity(createdShop, { model: strapi.models.shop });
    return {
      status: "success",
      sendEmail: true,
      type: "register-shop",
      data: {
        shop: sanitizeEntity(createdShop, { model: strapi.models.shop }),
        user: sanitizedCreatedShop.owner,
      },
      user: sanitizedCreatedShop.owner,
    };
  },
  update: async (ctx) => {
    const { id } = ctx.params;
    let shopObj = ctx.request.body;
    shopObj.slug = uniqid(
      slugify(shopObj.name, { replacement: "-", lower: true }) + "-"
    );
    let updatedShop = await strapi.services.shop.update(
      {
        id: id,
        tenant_id: ctx.request.header["x-auth-neotenant"],
        owner: ctx.state.user.id,
      },
      shopObj
    );
    return sanitizeEntity(updatedShop, { model: strapi.models.shop });
  },
  addImage: async (ctx) => {
    const { id } = ctx.params;
    let entity;
    if (ctx.is("multipart")) {
      const { data, files } = parseMultipartData(ctx);
      entity = await strapi.services.shop.update({ id }, data, {
        files,
      });
    } else {
      ctx.request.body.tenant_id = ctx.request.header["x-auth-neotenant"];
      entity = await strapi.services.shop.update({ id }, ctx.request.body);
    }

    return sanitizeEntity(entity, { model: strapi.models.shop });
  },

  findShopImages: async (ctx) => {
    ctx.query["id"] = ctx.params.id;
    return strapi.query("shop").find(ctx.query, ["images"]);
  },
  getDashboardData: async (ctx) => {
    const tenant_id = ctx.request.header["x-auth-neotenant"];
    let entities = {};
    let shopId = ctx.params.id;

    let knex = strapi.connections.default;

    //get total sales of order-items
    const querySalesTotal = `SELECT SUM(quantity*unit_price) as total FROM order_items WHERE shop = ? AND tenant_id = ?;`;
    let querySalesTotalResult = await knex.raw(querySalesTotal, [
      ctx.params.id,
      tenant_id,
    ]);
    entities.sales_amount_total = querySalesTotalResult.rows;
    entities.sales_amount_total[0].currency = "GHS";

    //get today sales of order-items
    const querySalesToday = `SELECT SUM(quantity*unit_price) as total FROM order_items WHERE shop = ? AND tenant_id = ? AND created_at > now()::date + interval '1h';`;
    let querySalesTodayResult = await knex.raw(querySalesToday, [
      ctx.params.id,
      tenant_id,
    ]);
    entities.sales_amount_today = querySalesTodayResult.rows;
    entities.sales_amount_today[0].currency = "GHS";

    //get count of orders of order-items
    const queryOrdersToday = `SELECT COUNT(DISTINCT oi.order) FROM order_items oi WHERE shop = ? AND status = ? AND tenant_id = ? AND created_at > now()::date + interval '1h';`;
    let queryOrdersTodayResult = await knex.raw(queryOrdersToday, [
      ctx.params.id,
      "completed",
      tenant_id,
    ]);
    entities.orders_count_completed_today = queryOrdersTodayResult.rows[0];

    //get count of pending orders of order-items
    const queryPendingOrdersToday = `SELECT COUNT(DISTINCT oi.order) FROM order_items oi WHERE shop = ? AND status = ? AND tenant_id = ? AND created_at > now()::date + interval '1h';`;
    let queryPendingOrdersTodayResult = await knex.raw(
      queryPendingOrdersToday,
      [ctx.params.id, "pending", tenant_id]
    );
    entities.orders_count_pending_today = queryPendingOrdersTodayResult.rows[0];

    //last few orders
    const queryLastFewOrders = `SELECT oi.id,oi.status,oi.name,oi.unit_price,oi.quantity,json_agg(json_build_object('image',uf.*)) images,'GHS' currency  FROM order_items oi 
         LEFT JOIN variations va ON va.id = oi.variation
LEFT JOIN products__images pi ON pi.product_id = va.product  LEFT JOIN upload_file_morph um ON um.related_id = "pi"."shop-image_id"
LEFT JOIN upload_file uf ON uf.id = um.upload_file_id WHERE shop = ? AND oi.tenant_id = ? GROUP BY oi.id ORDER BY oi.created_at DESC LIMIT ?;`;
    let queryLastFewOrdersTodayResult = await knex.raw(queryLastFewOrders, [
      ctx.params.id,
      tenant_id,
      6,
    ]);
    entities.orders_list_small = queryLastFewOrdersTodayResult.rows;

    //top products/variation (with orders complete)
    const queryTopProducts = `SELECT oi.name,SUM(oi.quantity),'GHS' currency,va.product FROM order_items oi LEFT JOIN variations va ON va.id = oi.variation
          WHERE shop = ? AND oi.status = ? AND oi.tenant_id = ? GROUP BY oi.name,va.product LIMIT 6;`;
    let queryTopProductsResult = await knex.raw(queryTopProducts, [
      ctx.params.id,
      "completed",
      tenant_id,
    ]);
    entities.products_list_top = queryTopProductsResult.rows;

    //sales per period range (with orders complete)
    const salesPeriod = "days";
    const querySalesPeriodList = `SELECT SUM(oi.quantity) qty,date_trunc('${salesPeriod}',oi.created_at) period,SUM(oi.quantity*oi.unit_price) FROM order_items oi WHERE shop = ? AND oi.status = ? AND tenant_id = ? GROUP BY 2 ORDER BY 2 DESC LIMIT 7;`;
    let querySalesPeriodListResult = await knex.raw(querySalesPeriodList, [
      ctx.params.id,
      "completed",
      tenant_id,
    ]);
    entities.sales_list_period = querySalesPeriodListResult.rows;

    //total product order quantity
    const queryTotalOrdersCount = `SELECT SUM(oi.quantity) count FROM order_items oi WHERE shop = ? AND oi.status = ? AND tenant_id = ?;`;
    let queryTotalOrdersCountResult = await knex.raw(queryTotalOrdersCount, [
      ctx.params.id,
      "completed",
      tenant_id,
    ]);
    entities.orders_count_total = queryTotalOrdersCountResult.rows[0];

    return entities;
  },
};
