"use strict";
const { parseMultipartData, sanitizeEntity } = require("strapi-utils");

module.exports = {
  async addImage(ctx, shopId) {
    let tenant_id = ctx.request.header["x-auth-neotenant"];

    if (Array.isArray(ctx.request.body)) {
      let dataBody = ctx.body;
      let imgData = [];
      for (let k in dataBody) {
        imgData.push({
          shop: shopId,
          image: dataBody[k],
          tenant_id: tenant_id,
        });
      }

      return await Promise.all(
        imgData.map(strapi.services["shop-image"].create)
      );
    } else {
      return strapi.services["shop-image"].create({
        shop: shopId,
        image: ctx.body,
        tenant_id: tenant_id,
      });
    }
  },
};
