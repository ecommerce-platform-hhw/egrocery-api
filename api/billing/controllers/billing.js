"use strict";
var _ = require("underscore");
const { parseMultipartData, sanitizeEntity } = require("strapi-utils");
/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  create: async (ctx) => {
    let userExists = await strapi
      .query("billing")
      .findOne({ user: ctx.state.user.id, tenant_id: ctx.query["tenant_id"] });

    let billingObj = ctx.request.body;
    billingObj.user = ctx.state.user.id;
    let billingRes = {};
    //if true user not found
    if (_.isEmpty(userExists))
      billingRes = await strapi.services.billing.create(billingObj);
    else
      billingRes = await strapi.services.billing.update(
        { id: userExists.id },
        billingObj
      );

    return sanitizeEntity(billingRes, { model: strapi.models.billing });
  },
  find: async (ctx) => {
    ctx.query["user"] = ctx.state.user.id;
    let entity = await strapi.query("billing").findOne(ctx.query);
    return sanitizeEntity(entity, { model: strapi.models.billing });
  },
  findOneAdminBilling: async (ctx) => {
    ctx.query["user"] = ctx.params.id;
    let entity = await strapi.query("billing").findOne(ctx.query);

    return sanitizeEntity(entity, { model: strapi.models.billing });
  },
};
