"use strict";

/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  findOrderDetailsForShop: async (ctx) => {
    ctx.query["id"] = ctx.params.id;
    ctx.query["shop.id"] = ctx.params.shopId;

    let order = await strapi
      .query("order-items")
      .findOne(ctx.query, ["variation.images.image"]);

    let relatedOrders = await strapi
      .query("order-items")
      .find({
        "order.id": order.order.id,
        "shop.id": ctx.params.shopId,
        id_ne: ctx.params.id,
      });

    return { ...order, ...{ related: relatedOrders } };
  },
  findAdminOrderDetails: async (ctx) => {
    ctx.query["order.id"] = ctx.params.id;
    let orderDetails = await strapi
      .query("order")
      .findOne({ id: ctx.params.id });
    let billingDetails = await strapi
      .query("order-billing")
      .findOne({ order: ctx.params.id });
    let orderItems = await strapi
      .query("order-items")
      .find(ctx.query, ["variation.images.image"]);

    return { items: orderItems, billing: billingDetails, order: orderDetails };
  },
  findAdminUserOrderDetails: async (ctx) => {
    ctx.query["order.id"] = ctx.params.id;
    let orderDetails = await strapi
      .query("order")
      .findOne({ id: ctx.params.id, buyer: ctx.params.user });

    if (!orderDetails) {
      return { items: [], billing: [], order: [] };
    }
    let billingDetails = await strapi
      .query("order-billing")
      .findOne({ order: ctx.params.id });
    let orderItems = await strapi
      .query("order-items")
      .find(ctx.query, ["variation.images.image"]);

    return { items: orderItems, billing: billingDetails, order: orderDetails };
  },

  findOrdersForShop: async (ctx) => {
    let limit = 20;
    let start = 0;
    ctx.query._start = ctx.query._start ? ctx.query._start - 1 : start;
    ctx.query._limit = limit;

    ctx.query["shop.id"] = ctx.params.shopId;
    return strapi
      .query("order-items")
      .find(ctx.query, ["variation.images.image"]);
  },

  findAdminOrderDetailsForShop: async (ctx) => {
    ctx.query["id"] = ctx.params.id;
    ctx.query["shop.id"] = ctx.params.shopId;

    let order = await strapi
      .query("order-items")
      .findOne(ctx.query, ["variation.images.image"]);

    let relatedOrders = await strapi
      .query("order-items")
      .find({
        "order.id": order.order,
        "shop.id": ctx.params.shopId,
        id_ne: ctx.params.id,
      });
    let billingDetails = await strapi
      .query("order-billing")
      .findOne({ order: order.order });

    return {
      ...order,
      ...{ related: relatedOrders },
      ...{ billing: billingDetails },
    };
  },

  findCustomerOrdersItems: async (ctx) => {
    ctx.query["order.id"] = ctx.params.id;
    ctx.query["order.buyer.id"] = ctx.state.user.id;
    let billingDetails = await strapi
      .query("order-billing")
      .findOne({ order: ctx.params.id });
    let orderItems = await strapi
      .query("order-items")
      .find(ctx.query, ["variation.images.image"]);

    return { items: orderItems, billing: billingDetails };
  },
};
