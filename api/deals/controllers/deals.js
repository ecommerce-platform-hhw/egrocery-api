"use strict";
const { parseMultipartData, sanitizeEntity } = require("strapi-utils");
var _ = require("underscore");
/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  /**
   * Create a record.
   *
   * @return {Object}
   */

  async create(ctx) {
    let entity;
    if (ctx.is("multipart")) {
      const { data, files } = parseMultipartData(ctx);
      entity = await strapi.services.deals.create(data, { files });
    } else {
      entity = await strapi.services.deals.create(ctx.request.body);
    }
    return sanitizeEntity(entity, { model: strapi.models.deals });
  },

  findOne: async (ctx) => {
    let product = await strapi
      .query("deals")
      .findOne({ id: ctx.params.id }, [
        "category",
        "category.name",
        "image",
        "products",
        "products.images",
        "products.images.image",
        "products.variations",
        "products.variations.images",
        "products.variations.images.image",
      ]);

    return product;
  },

  /**
   * Update a record.
   *
   * @return {Object}
   */

  async update(ctx) {
    const { id } = ctx.params;

    let entity;
    if (ctx.is("multipart")) {
      const { data, files } = parseMultipartData(ctx);
      entity = await strapi.services.deals.update({ id }, data, {
        files,
      });
    } else {
      entity = await strapi.services.deals.update({ id }, ctx.request.body);
    }

    return sanitizeEntity(entity, { model: strapi.models.deals });
  },

  /**
   * Delete a record.
   *
   * @return {Object}
   */

  async delete(ctx) {
    const { id } = ctx.params;

    const entity = await strapi.services.deals.delete({ id });
    return sanitizeEntity(entity, { model: strapi.models.deals });
  },

  findOneAdminDeals: async (ctx) => {
    let product = await strapi
      .query("deals")
      .findOne({ id: ctx.params.id }, [
        "category",
        "category.name",
        "image",
        "products",
        "products.images",
        "products.images.image",
        "products.variations",
        "products.variations.images",
        "products.variations.images.image",
      ]);

    return product;
  },

  findAdminDeals: async (ctx) => {
    let product = await strapi
      .query("deals")
      .find(ctx.query, [
        "category",
        "category.name",
        "image",
        "products",
        "products.images",
        "products.images.image",
        "products.variations",
        "products.variations.images",
        "products.variations.images.image",
      ]);

    return product;
  },
  updateDealProducts: async (ctx) => {
    const { id, type } = ctx.params;
    let dealObj = ctx.request.body;
    let deals = await strapi
      .query("deals")
      .findOne({ id: id }, [
        "products",
        "products.images",
        "products.images.image",
      ]);
    let updatedDealObj = [];
    if (type === "add")
      updatedDealObj = [...deals.products, ...dealObj.products];
    else if (type === "remove") {
      for (let prod of dealObj.products) {
        deals.products = _.without(
          deals.products,
          _.findWhere(deals.products, {
            id: prod.id,
          })
        );
      }

      updatedDealObj = deals.products;
    }

    let updatedDeal = await strapi.services.deals.update(
      { id: id, tenant_id: ctx.request.header["x-auth-neotenant"] },
      { products: updatedDealObj }
    );
    return sanitizeEntity(updatedDeal, { model: strapi.models.deals });
  },
};
