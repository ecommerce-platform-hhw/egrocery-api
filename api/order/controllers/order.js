"use strict";
const { parseMultipartData, sanitizeEntity } = require("strapi-utils");

module.exports = {
  find: async (ctx) => {
    let limit = 10;
    let start = 0;
    ctx.query._start = ctx.query._start ? ctx.query._start - 1 : start;
    ctx.query._limit = limit;
    return strapi.query("order").find(ctx.query);
  },
  findCustomerOrders: async (ctx) => {
    let limit = 10;
    let start = 0;
    ctx.query._start = ctx.query._start ? ctx.query._start - 1 : start;
    ctx.query._limit = limit;
    ctx.query["buyer.id"] = ctx.state.user.id;
    return strapi.query("order").find(ctx.query);
  },
  countCustomerOrders(ctx) {
    if (ctx.query._q) {
      return strapi.services.order.countSearch(ctx.query);
    }
    return strapi.services.order.count(ctx.query);
  },
  findAdminOrders: async (ctx) => {
    let limit = 10;
    let start = 0;
    ctx.query._start = ctx.query._start ? ctx.query._start - 1 : start;
    ctx.query._limit = limit;

    let orders = await strapi.query("order").find(ctx.query);

    //find order items
    if (orders.length > 0) {
      for (let i in orders) {
        let orderCountQuery = { order: orders[i].id };
        let orderCount = await strapi
          .query("order-items")
          .count(orderCountQuery);
        orders[i].product_count = orderCount;
      }
    }

    return orders;
  },
  findAdminUserOrders: async (ctx) => {
    ctx.query.buyer = ctx.params.user;
    let orders = await strapi.query("order").find(ctx.query);

    //find order items
    if (orders.length > 0) {
      for (let i in orders) {
        let orderCountQuery = { order: orders[i].id };
        let orderCount = await strapi
          .query("order-items")
          .count(orderCountQuery);
        orders[i].product_count = orderCount;
      }
    }

    return orders;
  },
  findOneAdminUserOrder: async (ctx) => {
    let orders = await strapi.query("order").findOne(ctx.query);

    //find order items
    if (orders.length > 0) {
      for (let i in orders) {
        let orderCountQuery = { order: orders[i].id };
        let orderCount = await strapi
          .query("order-items")
          .count(orderCountQuery);
        orders[i].product_count = orderCount;
      }
    }

    return orders;
  },
  async create(ctx) {
    let products = ctx.request.body.products;
    let billingInfo = ctx.request.body.billingInfo;
    let paymentInfo = ctx.request.body.paymentInfo;

    //get selected product and variation details
    let productDetails = products.map(async (detail) => {
      let id = detail.id;
      const entity = await strapi.services.variation.findOne({ id });

      let order = {
        qty: detail.qty,
        product: sanitizeEntity(entity, { model: strapi.models.variation }),
      };

      return order;
    });
    let productsAll = await Promise.all(productDetails);

    //calculate grandtotal
    let totalPrice = await calculateOrderTotalPrice(productsAll);

    let merchantFee = await calculateMerchantFee(totalPrice);
    let payout = await calculateOrderTotalPayout(totalPrice, merchantFee);

    let orderObj = {
      total_price: totalPrice,
      merchant_fee: merchantFee,
      payout,
      buyer: ctx.state.user.id,
      tenant_id: ctx.query["tenant_id"],
    };

    //make payment
    let createdOrder = null;
    let createdOrderItems = null;
    let createdOrderBilling = null;
    if (paymentInfo.type === "neo-wallet") {
      let user = ctx.state.user.id;
      let userBalance = await strapi.query("account-balance").findOne({ user });

      if (!userBalance)
        return ctx.throw(400, "Insufficient balance in your wallet");

      let newBalance = userBalance.balance - totalPrice;

      if (newBalance < 0)
        return ctx.throw(
          400,
          "Balance insufficient. Kindly topup or use another payment channel"
        );

      //create order
      createdOrder = await strapi.services.order.create(orderObj);

      //create order items
      let orderItemsObj = productsAll.map((item) => {
        return {
          name: item.product.name,
          slug: item.product.slug,
          unit_price: item.product.price,
          variation: item.product.id,
          quantity: item.qty,
          order: createdOrder.id,
          shop: item.product.product.shop,
          tenant_id: ctx.query["tenant_id"],
        };
      });
      createdOrderItems = await Promise.all(
        orderItemsObj.map(strapi.services["order-items"].create)
      );

      //create order billing details
      billingInfo.order = createdOrder.id;
      billingInfo.tenant_id = ctx.query["tenant_id"];
      createdOrderBilling = await strapi.services["order-billing"].create(
        billingInfo
      );

      let transactionObj = {
        type: "debit",
        payment_method: paymentInfo.type,
        amount: totalPrice,
        currency: "GHS",
        order: createdOrder.id.toString(),
        status: "pending",
        user: user,
        tenant_id: ctx.query["tenant_id"],
      };
      let transactionCreationResult = await strapi.services.transaction.create(
        transactionObj
      );
      let accountBalanceResult = await strapi.services[
        "account-balance"
      ].update({ id: userBalance.id, user: user }, { balance: newBalance });
      let transactionUpdateResult = await strapi.services.transaction.update(
        { id: transactionCreationResult.id },
        { status: "completed" }
      );
      let orderUpdateResult = await strapi.services.order.update(
        { id: createdOrder.id },
        { status: "paid" }
      );
    } else if (paymentInfo.type === "momo") {
      //create order
      createdOrder = await strapi.services.order.create(orderObj);

      //create order items
      let orderItemsObj = productsAll.map((item) => {
        return {
          name: item.product.name,
          slug: item.product.slug,
          unit_price: item.product.price,
          variation: item.product.id,
          quantity: item.qty,
          order: createdOrder.id,
          shop: item.product.product.shop,
          tenant_id: ctx.query["tenant_id"],
        };
      });
      let createdOrderItems = await Promise.all(
        orderItemsObj.map(strapi.services["order-items"].create)
      );

      //create order billing details
      billingInfo.order = createdOrder.id;
      let createdOrderBilling = await strapi.services["order-billing"].create(
        billingInfo
      );

      let transactionObj = {
        type: "debit",
        payment_method: paymentInfo.type,
        amount: totalPrice,
        currency: "GHS",
        order: createdOrder.id.toString(),
        status: "pending",
        user: user,
        tenant_id: ctx.query["tenant_id"],
      };
      let transactionCreationResult = await strapi.services.transaction.create(
        transactionObj
      );

      //  let transactionUpdateResult = await strapi.services.transaction.update({id:transactionCreationResult.id},{status:'completed'})
      //  let orderUpdateResult = await strapi.services.order.update({id:createdOrder.id},{status:'paid'})
    } else {
      return ctx.throw(
        400,
        "Payment method currently not supported. Sorry for the inconvenience"
      );
    }

    //format for sending emails
    //master api will send emails
    //slave api will send data to be sent via email in object wrapped as:
    //{status:'success',sendEmail:true,data : data}

    return {
      status: "success",
      sendEmail: true,
      type: "purchase-order",
      data: {
        order: sanitizeEntity(createdOrder, { model: strapi.models.order }),
        items: createdOrderItems,
        billing: createdOrderBilling,
      },
      user: createdOrder.buyer,
    };
  },
  createOrder: (ctx) => {
    return strapi
      .query("product")
      .find(ctx.query, [
        "category",
        "category.name",
        "variations",
        "variations.images",
        "images",
      ]);
  },
};

async function calculateOrderTotalPrice(order) {
  return new Promise((resolve) => {
    let initialValue = 0;
    let total = order.reduce((acc, val) => {
      let final_price = 0;
      if (val.product.discount_active) {
        final_price = val.product.discount;
      } else {
        final_price = val.product.price;
      }

      return acc + final_price * val.qty;
    }, initialValue);

    resolve(total);
  });
}

async function calculateMerchantFee(amount) {
  let merchantFeePercent = 0.01;

  return amount * merchantFeePercent;
}

async function calculateOrderTotalPayout(amount, fee) {
  return amount - fee;
}
