const request = require("supertest");
const { beforeAll, afterAll, describe, it, expect } = require("@jest/globals");
const { jwt, grantPrivilege } = require("../helpers/strapi");
const { createUser } = require("../user/factory");

let user;
let token;

beforeAll(async () => {
  user = await createUser();
  await grantPrivilege(1, "permissions.application.controllers.variation.find"); // 1 is default role for new confirmed users
  await grantPrivilege(
    1,
    "permissions.application.controllers.variation.findOne"
  );

  await grantPrivilege(
    1,
    "permissions.application.controllers.variation.create"
  );
  await grantPrivilege(
    1,
    "permissions.application.controllers.variation.update"
  );

  await grantPrivilege(
    1,
    "permissions.application.controllers.variation.delete"
  );

  token = jwt(user.id);
});

describe("Variation CRUD", () => {
  test("Should create variation", async () => {
    const variation = {
      name: "Variation 1",
      description: "Variation description",
      product: 1,
    };
    await request(strapi.server)
      .post("/variations")
      .send(variation)
      .set("Authorization", `Bearer ${token}`)
      .expect(200) // Expect response http code 200
      .then((data) => {
        expect.objectContaining({
          name: expect.any(String),
          description: expect.any(String),
        });
        // );
      });
  });

  test("Should return variations", async () => {
    const response = await request(strapi.server) // app server is an instance of Class: http.Server
      .get("/variations")
      .set("Authorization", `Bearer ${token}`)

      .expect(200) // Expect response http code 200
      .then((data) => {
        expect(Array.isArray(data.body)).toBe(true);
      });
  });

  test("Should return one variation", async () => {
    const response = await request(strapi.server) // app server is an instance of Class: http.Server
      .get("/variations/1")
      .set("Authorization", `Bearer ${token}`)

      .expect(200) // Expect response http code 200
      .then((data) => {
        expect(typeof data.body).toBe("object");
      });
  });

  test("Should update variation", async () => {
    const variation = {
      name: "Variation 1 updated",
      description: "Updated Variation description",
    };

    await request(strapi.server)
      .put(`/variations/1`, variation)
      .set("Authorization", `Bearer ${token}`)
      .expect(200); // Expect response http code 200
  });

  test("Should delete variation", async () => {
    await request(strapi.server)
      .delete(`/variations/1`)
      .set("Authorization", `Bearer ${token}`)
      .expect(200); // Expect response http code 200
  });
});
