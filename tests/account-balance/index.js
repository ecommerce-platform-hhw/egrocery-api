const request = require("supertest");
const { beforeAll, afterAll, describe, it, expect } = require("@jest/globals");
const { jwt, grantPrivilege } = require("../helpers/strapi");
const { createUser } = require("../user/factory");

let user;
let token;

beforeAll(async () => {
  user = await createUser();
  await grantPrivilege(
    1,
    "permissions.application.controllers['account-balance'].find"
  ); // 1 is default role for new confirmed users
  await grantPrivilege(
    1,
    "permissions.application.controllers['account-balance'].findOne"
  );

  await grantPrivilege(
    1,
    "permissions.application.controllers['account-balance'].create"
  );
  await grantPrivilege(
    1,
    "permissions.application.controllers['account-balance'].update"
  );

  await grantPrivilege(
    1,
    "permissions.application.controllers['account-balance'].delete"
  );

  token = jwt(user.id);
});

describe("Account balance CRUD", () => {
  test("Should create account-balance", async () => {
    const accountBalance = {
      balance: 20,
      currency: "USD",
    };
    await request(strapi.server)
      .post("/account-balances")
      .send(accountBalance)
      .set("Authorization", `Bearer ${token}`)
      .expect(200) // Expect response http code 200
      .then((data) => {
        expect.objectContaining({
          name: expect.any(String),
          description: expect.any(String),
        });
        // );
      });
  });
  test("Should return account-balances", async () => {
    const response = await request(strapi.server) // app server is an instance of Class: http.Server
      .get("/account-balances")
      .set("Authorization", `Bearer ${token}`)
      .expect(200) // Expect response http code 200
      .then((data) => {
        expect(Array.isArray(data.body)).toBe(true);
      });
  });
  test("Should return one account-balance", async () => {
    const response = await request(strapi.server) // app server is an instance of Class: http.Server
      .get("/account-balances/1")
      .set("Authorization", `Bearer ${token}`)
      .expect(200) // Expect response http code 200
      .then((data) => {
        expect(typeof data.body).toBe("object");
      });
  });
  test("Should update account-balance", async () => {
    const accountBalance = {
      name: "Account balance 1 updated",
      description: "Updated Account balance description",
      shop: 1,
    };
    await request(strapi.server)
      .put(`/account-balances/1`)
      .set("Authorization", `Bearer ${token}`)
      .send(accountBalance)
      .expect(200); // Expect response http code 200
  });
  test("Should delete account-balance", async () => {
    await request(strapi.server)
      .delete(`/account-balances/1`)
      .set("Authorization", `Bearer ${token}`)
      .expect(200); // Expect response http code 200
  });
});
