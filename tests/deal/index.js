const request = require("supertest");
const { beforeAll, afterAll, describe, it, expect } = require("@jest/globals");
const { jwt, grantPrivilege } = require("../helpers/strapi");
const { createUser } = require("../user/factory");

let user;
let token;

beforeAll(async () => {
  user = await createUser();
  await grantPrivilege(1, "permissions.application.controllers.deals.find"); // 1 is default role for new confirmed users
  await grantPrivilege(1, "permissions.application.controllers.deals.findOne");

  await grantPrivilege(1, "permissions.application.controllers.deals.create");
  await grantPrivilege(1, "permissions.application.controllers.deals.update");

  await grantPrivilege(1, "permissions.application.controllers.deals.delete");

  token = jwt(user.id);
});

describe("Deal CRUD", () => {
  test("Should create deal", async () => {
    const deal = {
      title: "Deal title",
      description: "Deal description",
    };
    await request(strapi.server)
      .post("/deals")
      .send(deal)
      .set("Authorization", `Bearer ${token}`)
      .expect(200) // Expect response http code 200
      .then((data) => {
        expect.objectContaining({
          name: expect.any(String),
          description: expect.any(String),
        });
        // );
      });
  });
  test("Should return deals", async () => {
    const response = await request(strapi.server) // app server is an instance of Class: http.Server
      .get("/deals")
      .set("Authorization", `Bearer ${token}`)
      .expect(200) // Expect response http code 200
      .then((data) => {
        expect(Array.isArray(data.body)).toBe(true);
      });
  });
  test("Should return one deal", async () => {
    const response = await request(strapi.server) // app server is an instance of Class: http.Server
      .get("/deals/1")
      .set("Authorization", `Bearer ${token}`)
      .expect(200) // Expect response http code 200
      .then((data) => {
        expect(typeof data.body).toBe("object");
      });
  });
  test("Should update deal", async () => {
    const deal = {
      name: "Deal 1 updated",
      description: "Updated Deal description",
      shop: 1,
      deal_relationship_tmp: [],
    };
    await request(strapi.server)
      .put(`/deals/1`)
      .set("Authorization", `Bearer ${token}`)
      .send(deal)
      .expect(200); // Expect response http code 200
  });
  test("Should delete deal", async () => {
    await request(strapi.server)
      .delete(`/deals/1`)
      .set("Authorization", `Bearer ${token}`)
      .expect(200); // Expect response http code 200
  });
});
