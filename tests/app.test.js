const fs = require("fs");
const { setupStrapi, cleanupStrapi } = require("./helpers/strapi");

// require("../extensions/users-permissions/tests/user");
jest.setTimeout(30000);
beforeAll(async () => {
  await setupStrapi();
});

/** this code is called once before all the tested are finished */
afterAll(async () => {
  const dbSettings = strapi.config.get("database.connections.default.settings");

  //close server to release the db-file
  await strapi.destroy();

  //delete test database after all tests
  if (dbSettings && dbSettings.filename) {
    const tmpDbFile = `${__dirname}/../${dbSettings.filename}`;
    if (fs.existsSync(tmpDbFile)) {
      fs.unlinkSync(tmpDbFile);
    }
  }
});

it("strapi is defined", () => {
  expect(strapi).toBeDefined();
});

require("./user");
require("./variation");
require("./product");
require("./shop");
require("./deal");
require("./account-balance");
