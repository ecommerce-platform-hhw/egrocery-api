const request = require("supertest");
const { beforeAll, afterAll, describe, it, expect } = require("@jest/globals");
const { jwt, grantPrivilege } = require("../helpers/strapi");
const { createUser } = require("../user/factory");

let user;
let token;

beforeAll(async () => {
  user = await createUser();
  await grantPrivilege(1, "permissions.application.controllers.product.find"); // 1 is default role for new confirmed users
  await grantPrivilege(
    1,
    "permissions.application.controllers.product.findOne"
  );

  await grantPrivilege(1, "permissions.application.controllers.product.create");
  await grantPrivilege(1, "permissions.application.controllers.product.update");

  await grantPrivilege(1, "permissions.application.controllers.product.delete");

  token = jwt(user.id);
});

describe("Product CRUD", () => {
  test("Should create product", async () => {
    const product = {
      name: "Product 1",
      description: "Product description",
      shop: 1,
    };
    await request(strapi.server)
      .post("/products")
      .send(product)
      .set("Authorization", `Bearer ${token}`)
      .expect(200) // Expect response http code 200
      .then((data) => {
        expect.objectContaining({
          name: expect.any(String),
          description: expect.any(String),
        });
        // );
      });
  });

  test("Should return products", async () => {
    const response = await request(strapi.server) // app server is an instance of Class: http.Server
      .get("/products")
      .set("Authorization", `Bearer ${token}`)

      .expect(200) // Expect response http code 200
      .then((data) => {
        expect(Array.isArray(data.body)).toBe(true);
      });
  });

  test("Should return one product", async () => {
    const response = await request(strapi.server) // app server is an instance of Class: http.Server
      .get("/products/1")
      .set("Authorization", `Bearer ${token}`)

      .expect(200) // Expect response http code 200
      .then((data) => {
        expect(typeof data.body).toBe("object");
      });
  });

  test("Should update product", async () => {
    const product = {
      name: "Product 1 updated",
      description: "Updated Product description",
      shop: 1,
      product_relationship_tmp: [],
    };

    await request(strapi.server)
      .put(`/products/1`)
      .set("Authorization", `Bearer ${token}`)
      .send(product)
      .expect(200); // Expect response http code 200
  });

  test("Should delete product", async () => {
    await request(strapi.server)
      .delete(`/products/1`)
      .set("Authorization", `Bearer ${token}`)
      .expect(200); // Expect response http code 200
  });
});
