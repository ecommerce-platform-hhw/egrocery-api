const request = require("supertest");
const { beforeAll, afterAll, describe, it, expect } = require("@jest/globals");
const { jwt, grantPrivilege } = require("../helpers/strapi");
const { createUser } = require("../user/factory");

let user;
let token;

beforeAll(async () => {
  user = await createUser();
  await grantPrivilege(1, "permissions.application.controllers.shop.find"); // 1 is default role for new confirmed users
  await grantPrivilege(1, "permissions.application.controllers.shop.findOne");

  await grantPrivilege(1, "permissions.application.controllers.shop.create");
  await grantPrivilege(1, "permissions.application.controllers.shop.update");

  await grantPrivilege(1, "permissions.application.controllers.shop.delete");

  token = jwt(user.id);
});

describe("Shop CRUD", () => {
  test("Should create shop", async () => {
    const shop = {
      name: "Shop 1",
      description: "Shop description",
    };
    await request(strapi.server)
      .post("/shops")
      .send(shop)
      .set("Authorization", `Bearer ${token}`)
      .set("x-auth-neotenant", "neotenant123")
      .expect(200) // Expect response http code 200
      .then((data) => {
        expect.objectContaining({
          name: expect.any(String),
          description: expect.any(String),
        });
        // );
      });
  });

  test("Should return shops", async () => {
    const response = await request(strapi.server) // app server is an instance of Class: http.Server
      .get("/shops")
      .set("Authorization", `Bearer ${token}`)

      .expect(200) // Expect response http code 200
      .then((data) => {
        expect(Array.isArray(data.body)).toBe(true);
      });
  });

  test("Should return one shop", async () => {
    const response = await request(strapi.server) // app server is an instance of Class: http.Server
      .get("/shops/1")
      .set("Authorization", `Bearer ${token}`)

      .expect(200) // Expect response http code 200
      .then((data) => {
        expect(typeof data.body).toBe("object");
      });
  });

  test("Should update shop", async () => {
    const shop = {
      name: "Shop 1 updated",
      description: "Updated Shop description",
    };

    await request(strapi.server)
      .put(`/shops/1`)
      .set("Authorization", `Bearer ${token}`)
      .set("x-auth-neotenant", "neotenant123")
      .send(shop)
      .expect(200); // Expect response http code 200
  });

  test("Should delete shop", async () => {
    await request(strapi.server)
      .delete(`/shops/1`)
      .set("Authorization", `Bearer ${token}`)
      .expect(200); // Expect response http code 200
  });
});
