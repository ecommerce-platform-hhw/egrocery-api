const emailRegExp =
  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
/* eslint-disable no-useless-escape */
const crypto = require("crypto");
const _ = require("lodash");
const grant = require("grant-koa");
const { sanitizeEntity } = require("strapi-utils");
const formatError = (error) => [
  { messages: [{ id: error.id, message: error.message, field: error.field }] },
];
module.exports = {
  async forgotPasswordVendor(ctx) {
    let { email } = ctx.request.body;

    // Check if the provided email is valid or not.
    const isEmail = emailRegExp.test(email);

    if (isEmail) {
      email = email.toLowerCase();
    } else {
      return ctx.badRequest(
        null,
        formatError({
          id: "Auth.form.error.email.format",
          message: "Please provide valid email address.",
        })
      );
    }

    // CUSTOM: ensure header contains x-auth-neotenant
    if (!ctx.request.header["x-auth-neotenant"]) {
      return ctx.unauthorized(`Tenant id not present in header`);
    }

    let tenant_id = ctx.request.header["x-auth-neotenant"];

    const pluginStore = await strapi.store({
      environment: "",
      type: "plugin",
      name: "users-permissions",
    });

    // Find the user by email.
    const user = await strapi
      .query("user", "users-permissions")
      .findOne({ email, tenant_id });

    // User not found.
    if (!user) {
      return ctx.badRequest(
        null,
        formatError({
          id: "Auth.form.error.user.not-exist",
          message: "This email does not exist.",
        })
      );
    }

    // Generate random token.
    const resetPasswordToken = crypto.randomBytes(64).toString("hex");

    const settings = await pluginStore
      .get({ key: "email" })
      .then((storeEmail) => {
        try {
          return storeEmail["reset_password"].options;
        } catch (error) {
          return {};
        }
      });

    const advanced = await pluginStore.get({
      key: "advanced",
    });

    const userInfo = _.omit(user, [
      "password",
      "resetPasswordToken",
      "role",
      "provider",
    ]);

    // URL: advanced.email_reset_password,
    settings.message = await strapi.plugins[
      "users-permissions"
    ].services.userspermissions.template(settings.message, {
      URL: `${strapi.config.server.url}/reset-password`,
      USER: userInfo,
      TOKEN: resetPasswordToken,
    });

    settings.object = await strapi.plugins[
      "users-permissions"
    ].services.userspermissions.template(settings.object, {
      USER: userInfo,
    });

    // Update the user.
    await strapi
      .query("user", "users-permissions")
      .update({ id: user.id, tenant_id: tenant_id }, { resetPasswordToken });

    // ctx.send({ ok: true });
    ctx.send({
      status: "success",
      sendEmail: true,
      type: "reset-password-vendor",
      data: {
        resetPasswordToken: resetPasswordToken,
        user: { username: user.username, email: user.email },
      },
      user: { username: user.username, email: user.email },
    });
  },
  async registerVendor(ctx) {
    const pluginStore = await strapi.store({
      environment: "",
      type: "plugin",
      name: "users-permissions",
    });

    const settings = await pluginStore.get({
      key: "advanced",
    });

    if (!settings.allow_register) {
      return ctx.badRequest(
        null,
        formatError({
          id: "Auth.advanced.allow_register",
          message: "Register action is currently disabled.",
        })
      );
    }

    const params = {
      ..._.omit(ctx.request.body, ["confirmed", "resetPasswordToken"]),
      provider: "local",
    };

    // CUSTOM: ensure header contains x-auth-neotenant
    if (!ctx.request.header["x-auth-neotenant"]) {
      return ctx.unauthorized(`Tenant id not present in header`);
    }

    let tenant_id = ctx.request.header["x-auth-neotenant"];

    // Password is required.
    if (!params.password) {
      return ctx.badRequest(
        null,
        formatError({
          id: "Auth.form.error.password.provide",
          message: "Please provide your password.",
        })
      );
    }

    // Email is required.
    if (!params.email) {
      return ctx.badRequest(
        null,
        formatError({
          id: "Auth.form.error.email.provide",
          message: "Please provide your email.",
        })
      );
    }

    // Throw an error if the password selected by the user
    // contains more than two times the symbol '$'.
    if (
      strapi.plugins["users-permissions"].services.user.isHashed(
        params.password
      )
    ) {
      return ctx.badRequest(
        null,
        formatError({
          id: "Auth.form.error.password.format",
          message:
            "Your password cannot contain more than three times the symbol `$`.",
        })
      );
    }

    const role = await strapi
      .query("role", "users-permissions")
      .findOne({ type: settings.default_role }, []);

    if (!role) {
      return ctx.badRequest(
        null,
        formatError({
          id: "Auth.form.error.role.notFound",
          message: "Impossible to find the default role.",
        })
      );
    }

    // Check if the provided email is valid or not.
    const isEmail = emailRegExp.test(params.email);

    if (isEmail) {
      params.email = params.email.toLowerCase();
    } else {
      return ctx.badRequest(
        null,
        formatError({
          id: "Auth.form.error.email.format",
          message: "Please provide valid email address.",
        })
      );
    }

    params.role = role.id;
    params.password = await strapi.plugins[
      "users-permissions"
    ].services.user.hashPassword(params);

    const user = await strapi.query("user", "users-permissions").findOne({
      email: params.email,
      tenant_id: tenant_id,
    });

    if (user && user.provider === params.provider) {
      return ctx.badRequest(
        null,
        formatError({
          id: "Auth.form.error.email.taken",
          message: "Email is already taken.",
        })
      );
    }

    if (user && user.provider !== params.provider && settings.unique_email) {
      return ctx.badRequest(
        null,
        formatError({
          id: "Auth.form.error.email.taken",
          message: "Email is already taken.",
        })
      );
    }

    try {
      if (!settings.email_confirmation) {
        params.confirmed = true;
      }

      //CUSTOMISATION: add tenant_id to params
      params.tenant_id = tenant_id;

      const user = await strapi
        .query("user", "users-permissions")
        .create(params);

      const jwt = strapi.plugins["users-permissions"].services.jwt.issue(
        _.pick(user.toJSON ? user.toJSON() : user, ["id"])
      );

      if (settings.email_confirmation) {
        const settings = await pluginStore
          .get({ key: "email" })
          .then((storeEmail) => {
            try {
              return storeEmail["email_confirmation"].options;
            } catch (error) {
              return {};
            }
          });

        settings.message = await strapi.plugins[
          "users-permissions"
        ].services.userspermissions.template(settings.message, {
          URL: `${strapi.config.server.url}/users-permissions/shop/email-confirmation`,
          USER: _.omit(user.toJSON ? user.toJSON() : user, [
            "password",
            "resetPasswordToken",
            "role",
            "provider",
          ]),
          CODE: jwt,
        });

        settings.object = await strapi.plugins[
          "users-permissions"
        ].services.userspermissions.template(settings.object, {
          USER: _.omit(user.toJSON ? user.toJSON() : user, [
            "password",
            "resetPasswordToken",
            "role",
            "provider",
          ]),
        });

        try {
          // Send an email to the user.
          await strapi.plugins["email"].services.email.send({
            to: (user.toJSON ? user.toJSON() : user).email,
            from:
              settings.from.email && settings.from.name
                ? `${settings.from.name} <${settings.from.email}>`
                : undefined,
            replyTo: settings.response_email,
            subject: settings.object,
            text: settings.message,
            html: settings.message,
          });
        } catch (err) {
          return ctx.badRequest(null, err);
        }
      }

      const sanitizedUser = sanitizeEntity(user.toJSON ? user.toJSON() : user, {
        model: strapi.query("user", "users-permissions").model,
      });
      if (settings.email_confirmation) {
        // ctx.send({
        //   user: sanitizedUser,
        // });
        ctx.send({
          status: "success",
          sendEmail: true,
          type: "register-vendor",
          data: { user: sanitizedUser },
          user: sanitizedUser,
        });
      } else {
        // ctx.send({
        //   jwt,
        //   user: sanitizedUser,
        // });

        ctx.send({
          status: "success",
          sendEmail: true,
          type: "register-vendor",
          data: { jwt, user: sanitizedUser },
          user: sanitizedUser,
        });
      }
    } catch (err) {
      const adminError = _.includes(err.message, "username")
        ? {
            id: "Auth.form.error.username.taken",
            message: "Username already taken",
          }
        : { id: "Auth.form.error.email.taken", message: "Email already taken" };

      ctx.badRequest(null, formatError(adminError));
    }
  },

  async emailConfirmation(ctx, next, returnUser) {
    const params = ctx.query;

    const decodedToken = await strapi.plugins[
      "users-permissions"
    ].services.jwt.verify(params.confirmation);

    let user = await strapi.plugins["users-permissions"].services.user.edit(
      { id: decodedToken.id },
      { confirmed: true }
    );

    if (returnUser) {
      ctx.send({
        jwt: strapi.plugins["users-permissions"].services.jwt.issue({
          id: user.id,
        }),
        user: sanitizeEntity(user.toJSON ? user.toJSON() : user, {
          model: strapi.query("user", "users-permissions").model,
        }),
      });
    } else {
      const settings = await strapi
        .store({
          environment: "",
          type: "plugin",
          name: "users-permissions",
          key: "advanced",
        })
        .get();

      //ctx.redirect(settings.email_confirmation_redirection || '/');
      ctx.redirect("http://localhost:4200/login");
    }
  },

  async findAdminUsers(ctx) {
    return await strapi.plugins["users-permissions"].controllers.user.find(ctx);
  },
  async findOneAdminUserDetails(ctx) {
    return await strapi.plugins["users-permissions"].controllers.user.findOne(
      ctx
    );
  },
};
