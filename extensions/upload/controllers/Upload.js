"use strict";

/**
 * Upload.js controller
 *
 */

const _ = require("lodash");
const validateSettings = require("../../../node_modules/strapi-plugin-upload/controllers/validation/settings");
const { yup, formatYupErrors } = require("strapi-utils");

const fileInfoSchema = yup.object({
  name: yup.string().nullable(),
  alternativeText: yup.string().nullable(),
  caption: yup.string().nullable(),
  provider_metadata: yup.object({}),
});

const uploadSchema = yup.object({
  fileInfo: fileInfoSchema,
});

const multiUploadSchema = yup.object({
  fileInfo: yup.array().of(fileInfoSchema),
});

const validateUploadBody = (schema, data = {}) => {
  return schema.validate(data, { abortEarly: false }).catch((err) => {
    throw strapi.errors.badRequest("ValidationError", {
      errors: formatYupErrors(err),
    });
  });
};

const isUploadDisabled = () =>
  _.get(strapi.plugins, "upload.config.enabled", true) === false;

const disabledPluginError = () =>
  strapi.errors.badRequest(null, {
    errors: [
      { id: "Upload.status.disabled", message: "File upload is disabled" },
    ],
  });

const emptyFileError = () =>
  strapi.errors.badRequest(null, {
    errors: [{ id: "Upload.status.empty", message: "Files are empty" }],
  });

module.exports = {
  async upload(ctx) {
    if (isUploadDisabled()) {
      throw disabledPluginError();
    }

    const { id } = ctx.query;

    //note: very important: before neocommerce-manager the commented line below was valid but for some reason
    //axios was not sending ctx.request.files and hence ctx.request.body.picfiles.
    // const files = _.get(ctx.request.files, 'files');
    const files = _.get(ctx.request.body.picfiles, "files");

    ctx.request.body.provider_metadata = ctx.request.body.files
      ? JSON.parse(ctx.request.body.files)
      : "";

    // update only fileInfo if not file content sent
    if (id && (_.isEmpty(files) || files.size === 0)) {
      const data = await validateUploadBody(uploadSchema, ctx.request.body);

      ctx.body = await strapi.plugins.upload.services.upload.updateFileInfo(
        id,
        data.fileInfo
      );
      return;
    }

    if (_.isEmpty(files) || files.size === 0) {
      throw emptyFileError();
    }
    const uploadService = strapi.plugins.upload.services.upload;

    const validationSchema = Array.isArray(files)
      ? multiUploadSchema
      : uploadSchema;
    const data = await validateUploadBody(validationSchema, ctx.request.body);

    if (id) {
      // cannot replace with more than one file
      if (Array.isArray(files)) {
        throw strapi.errors.badRequest(null, {
          errors: [
            {
              id: "Upload.replace.single",
              message: "Cannot replace a file with multiple ones",
            },
          ],
        });
      }
      ctx.body = await uploadService.replace(id, { data, file: files });
    } else {
      ctx.body = await uploadService.upload({ data, files });

      if (ctx.request.body.files && JSON.parse(ctx.request.body.files).shop) {
        strapi.api.shop.services.shop.addImage(
          ctx,
          JSON.parse(ctx.request.body.files).shop
        );
      }
    }
  },
};
