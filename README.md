# eGrocery Core API

This application provides an api for the eGrocery ecommerce platform.

## Install dependencies

### `npm install`

## Available Scripts

For local development, create your postgres db and configure in `database.js` in the `config` directory else server will not run

In the project directory, you can run:

### `yarn develop`

Runs the app in the development mode.

In production, run the `ecosystem.config.js`

### `npm test`

Launches the test runner via jest. To generate coverage file, add `--coverage` to test runner in `package.json`

### `npm run build`

Builds the admin interface for production to the `build` folder.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

## Configuration

See `config folder` and `.env.example` file
